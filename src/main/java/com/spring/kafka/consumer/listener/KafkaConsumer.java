package com.spring.kafka.consumer.listener;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {

  @KafkaListener(topics = "store_new_order", groupId = "consumer")
  public void listen(ConsumerRecord<?, ?> consumerRecord, Acknowledgment acknowledgment) {

    System.out.println("Received Message: " + consumerRecord.value().toString());
    acknowledgment.acknowledge();
  }

}
